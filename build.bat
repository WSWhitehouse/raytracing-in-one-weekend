@echo off

set IncludeDirs=-I..\src -I..\vendor
set CompilerFlags=-O0 -Wall -std=c++17
set Libs=
set LinkerFlags=-Wl,-subsystem:console

echo Build Started...
pushd build
clang %CompilerFlags% %IncludeDirs% ..\src\main.cpp -o raytracing.exe %Libs% %LinkerFlags%
popd build
echo Build finished.