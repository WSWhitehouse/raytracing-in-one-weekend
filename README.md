# raytracing-in-one-weekend

Ray Tracing in One Weekend: [raytracing.github.io](https://raytracing.github.io).

## Supported Platforms
- Windows 64-bit
- Windows 32-bit (untested, but can't see why it wouldn't work)

## Building/Running the Application
Run `build.bat` to generate the executable, requires clang to be installed and located on the path. Build files are located inside the `build` folder. To run the application run the `run.bat` file, this ensures the executable has the iostream/cout redirected to `image.ppm`. The image output file is overwritten everytime the application is run. 

## Assets
Assets are stored in the `assets` folder. The program is designed to be run using `run.bat`, if this is not the case then assets will not be found correctly. The run bat file stays in the root directory of the project while running the exe, this way the program can have access to the assets folder. In code the file path to assets is `./assets/asset_filename.extension` (of course assets could also be in subfolders).

## Images
Images from milestones or ones I particularly like are located in the `images` folder. 
