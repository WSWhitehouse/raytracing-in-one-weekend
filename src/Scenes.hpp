#ifndef SCENES_HPP
#define SCENES_HPP

#include "Common.hpp"
#include "Random.hpp"
#include "Camera.hpp"
#include "ImageData.hpp"

// Hittable
#include "hittable/HittableList.hpp"
#include "hittable/Sphere.hpp"
#include "hittable/MovingSphere.hpp"
#include "hittable/AARect.hpp"
#include "hittable/Box.hpp"
#include "hittable/BVH.hpp"
#include "hittable/ConstantMedium.hpp"

#include "hittable/Translate.hpp"
#include "hittable/Rotate.hpp"

// Materials
#include "material/Material.hpp"
#include "material/Lambertian.hpp"
#include "material/Dielectric.hpp"
#include "material/Metal.hpp"
#include "material/DiffuseLight.hpp"

// Textures
#include "textures/Texture.hpp"
#include "textures/SolidColour.hpp"
#include "textures/CheckerTexture.hpp"
#include "textures/NoiseTexture.hpp"
#include "textures/ImageTexture.hpp"

class Scenes
{
 public: 
  static void Default(HittableList& world, CameraData& camData)
  {
    camData = GetDefaultCameraData();

    // Materials
    std::shared_ptr<Material> matGround = std::make_shared<Lambertian>(Colour(0.8, 0.8, 0.0));
    std::shared_ptr<Material> matCenter = std::make_shared<Lambertian>(Colour(0.1, 0.2, 0.5));
    std::shared_ptr<Material> matLeft   = std::make_shared<Dielectric>(1.5);
    std::shared_ptr<Material> matRight  = std::make_shared<Metal>(Colour(0.8, 0.6, 0.2), 0.0);

    // World
    world.Add(std::make_shared<Sphere>(Point3( 0.0, -100.5, -1.0), 100.0, matGround));
    world.Add(std::make_shared<Sphere>(Point3( 0.0,    0.0, -1.0),   0.5, matCenter));
    world.Add(std::make_shared<Sphere>(Point3(-1.0,    0.0, -1.0),   0.5, matLeft));
    world.Add(std::make_shared<Sphere>(Point3(-1.0,    0.0, -1.0),  -0.4, matLeft));
    world.Add(std::make_shared<Sphere>(Point3( 1.0,    0.0, -1.0),   0.5, matRight));
  }

  static void RandomSpheres(HittableList& world, CameraData& camData, Colour& background) 
  {
    camData    = GetDefaultCameraData();
    background = Colour(0.7, 0.8, 1.0);

    std::shared_ptr<Texture> groundTexture = std::make_shared<CheckerTexture>(Colour(0.2, 0.3, 0.1), Colour(0.9, 0.9, 0.9));
    world.Add(std::make_shared<Sphere>(Point3(0,-1000,0), 1000, std::make_shared<Lambertian>(groundTexture)));

    for (int a = -11; a < 11; a++) 
    {
      for (int b = -11; b < 11; b++) 
      {
        float64 chooseMat = Random::Range();
        Point3 center(a + 0.9*Random::Range(), 0.2, b + 0.9 * Random::Range());

        if ((center - Point3(4, 0.2, 0)).Length() > 0.9) 
        {
          std::shared_ptr<Material> mat;

          if (chooseMat < 0.8)
          {
            // diffuse
            Colour albedo = RandomVec3() * RandomVec3();
            mat = std::make_shared<Lambertian>(albedo);
          } 
          else if (chooseMat < 0.95) 
          {
            // metal
            Colour albedo = RandomVec3(0.5, 1);
            double fuzz   = Random::Range<double>(0.0, 0.5);
            mat = std::make_shared<Metal>(albedo, fuzz);
          } 
          else
          {
            // glass
            mat = std::make_shared<Dielectric>(1.5);
          }

          world.Add(std::make_shared<Sphere>(center, 0.2, mat));
        }
      }
    }

    auto material1 = std::make_shared<Dielectric>(1.5);
    world.Add(std::make_shared<Sphere>(Point3(0, 1, 0), 1.0, material1));

    auto material2 = std::make_shared<Lambertian>(Colour(0.4, 0.2, 0.1));
    world.Add(std::make_shared<Sphere>(Point3(-4, 1, 0), 1.0, material2));

    auto material3 = std::make_shared<Metal>(Colour(0.7, 0.6, 0.5), 0.0);
    world.Add(std::make_shared<Sphere>(Point3(4, 1, 0), 1.0, material3));
  }

  static void RandomMovingSpheres(HittableList& world, CameraData& camData, Colour& background) 
  {
    camData    = GetDefaultCameraData();
    background = Colour(0.7, 0.8, 1.0);

    std::shared_ptr<Texture> groundTexture = std::make_shared<CheckerTexture>(Colour(0.2, 0.3, 0.1), Colour(0.9, 0.9, 0.9));
    world.Add(std::make_shared<Sphere>(Point3(0,-1000,0), 1000, std::make_shared<Lambertian>(groundTexture)));

    for (int a = -11; a < 11; a++) 
    {
      for (int b = -11; b < 11; b++) 
      {
        float64 chooseMat = Random::Range();
        Point3 center(a + 0.9*Random::Range(), 0.2, b + 0.9*Random::Range());

        if ((center - Point3(4, 0.2, 0)).Length() > 0.9) 
        {
          std::shared_ptr<Material> mat;

          if (chooseMat < 0.8)
          {
            // diffuse
            Colour albedo = RandomVec3() * RandomVec3();
            mat = std::make_shared<Lambertian>(albedo);
          } 
          else if (chooseMat < 0.95) 
          {
            // metal
            Colour albedo = RandomVec3(0.5, 1);
            double fuzz   = Random::Range<double>(0.0, 0.5);
            mat = std::make_shared<Metal>(albedo, fuzz);
          } 
          else
          {
            // glass
            mat = std::make_shared<Dielectric>(1.5);
          }

          bool32 isMovingSphere = Random::Range() > 0.5;
          if (isMovingSphere)
          {
            Vec3 center2 = center + Vec3(0, Random::Range<float64>(0, 0.5), 0);
            world.Add(std::make_shared<MovingSphere>(center, center2, 0.0, 1.0, 0.2, mat));
          }
          else
          {
            world.Add(std::make_shared<Sphere>(center, 0.2, mat));
          }          
        }
      }
    }

    auto material1 = std::make_shared<Dielectric>(1.5);
    world.Add(std::make_shared<Sphere>(Point3(0, 1, 0), 1.0, material1));

    auto material2 = std::make_shared<Lambertian>(Colour(0.4, 0.2, 0.1));
    world.Add(std::make_shared<Sphere>(Point3(-4, 1, 0), 1.0, material2));

    auto material3 = std::make_shared<Metal>(Colour(0.7, 0.6, 0.5), 0.0);
    world.Add(std::make_shared<Sphere>(Point3(4, 1, 0), 1.0, material3));
  }

  static void TwoSpheres(HittableList& world, CameraData& camData, Colour& background)
  {
    camData = GetDefaultCameraData();
    camData.position = Point3(26.0, 3.0, 6.0);
    camData.lookAt   = Point3(0.0, 2.0, 0.0);

    background = Colour(0.7, 0.8, 1.0);

    std::shared_ptr<Texture> texture = std::make_shared<NoiseTexture>(4);
    world.Add(std::make_shared<Sphere>(Point3(0,-1000,0), 1000, std::make_shared<Lambertian>(texture)));
    world.Add(std::make_shared<Sphere>(Point3(0, 2, 0), 2, std::make_shared<Lambertian>(texture)));
  }

  static void SimpleLight(HittableList& world, CameraData& camData, Colour& background)
  {
    TwoSpheres(world, camData, background);

    background = Colour(0.0, 0.0, 0.0);

    std::shared_ptr<DiffuseLight> light = std::make_shared<DiffuseLight>(Colour(4.0, 4.0, 4.0));
    world.Add(std::make_shared<XYRect>(3, 5, 1, 3, -2, light));
    world.Add(std::make_shared<Sphere>(Point3(0, 6, 0), 1.5, light));
  }

  static void Earth(HittableList& world, CameraData& camData, Colour& background)
  {
    camData    = GetDefaultCameraData();
    background = Colour(0.7, 0.8, 1.0);
    
    std::shared_ptr<Texture> earthTexture   = std::make_shared<ImageTexture>("./assets/earthmap.jpg");
    std::shared_ptr<Material> earthMaterial = std::make_shared<Lambertian>(earthTexture);

    world.Add(std::make_shared<Sphere>(Point3(0.0, 0.0, 0.0), 2, earthMaterial));
  }

  static void CornellBox(HittableList& world, CameraData& camData, Colour& background)
  {
    camData = GetDefaultCameraData();
    camData.position    = Point3(278, 278, -800);
    camData.lookAt      = Point3(278, 278, 0);
    camData.viewUp      = Vec3(0.0, 1.0, 0.0);
    camData.verticalFOV = 40.0;

    background = Colour(0.0, 0.0, 0.0); 

    // Materials
    std::shared_ptr<Material> red   = std::make_shared<Lambertian>(Colour(0.65, 0.05, 0.05));
    std::shared_ptr<Material> white = std::make_shared<Lambertian>(Colour(0.73, 0.73, 0.73));
    std::shared_ptr<Material> green = std::make_shared<Lambertian>(Colour(0.12, 0.45, 0.15));
    std::shared_ptr<Material> light = std::make_shared<DiffuseLight>(Colour(15, 15, 15));

    // Walls
    world.Add(std::make_shared<YZRect>(  0, 555,   0, 555, 555, green));
    world.Add(std::make_shared<YZRect>(  0, 555,   0, 555,   0, red));
    world.Add(std::make_shared<XZRect>(213, 343, 227, 332, 554, light));
    world.Add(std::make_shared<XZRect>(  0, 555,   0, 555,   0, white));
    world.Add(std::make_shared<XZRect>(  0, 555,   0, 555, 555, white));
    world.Add(std::make_shared<XYRect>(  0, 555,   0, 555, 555, white));

    // Boxes
    std::shared_ptr<Hittable> box1 = std::make_shared<Box>(Point3(0.0, 0.0, 0.0), Point3(165.0, 330.0, 165.0), white);
    box1 = std::make_shared<RotateY>(box1, 15.0);
    box1 = std::make_shared<Translate>(box1, Vec3(265.0, 0.0, 295.0));

    std::shared_ptr<Hittable> box2 = std::make_shared<Box>(Point3(0.0, 0.0, 0.0), Point3(165.0, 165.0, 165.0), white);
    box2 = std::make_shared<RotateY>(box2, -18.0);
    box2 = std::make_shared<Translate>(box2, Vec3(130.0, 0.0, 65.0));

    world.Add(box1);
    world.Add(box2);
  }

  static void CornellSmoke(HittableList& world, CameraData& camData, Colour& background)
  {
    camData = GetDefaultCameraData();
    camData.position    = Point3(278, 278, -800);
    camData.lookAt      = Point3(278, 278, 0);
    camData.viewUp      = Vec3(0.0, 1.0, 0.0);
    camData.verticalFOV = 40.0;

    background = Colour(0.0, 0.0, 0.0); 

    // Materials
    std::shared_ptr<Material> red   = std::make_shared<Lambertian>(Colour(0.65, 0.05, 0.05));
    std::shared_ptr<Material> white = std::make_shared<Lambertian>(Colour(0.73, 0.73, 0.73));
    std::shared_ptr<Material> green = std::make_shared<Lambertian>(Colour(0.12, 0.45, 0.15));
    std::shared_ptr<Material> light = std::make_shared<DiffuseLight>(Colour(15, 15, 15));

    // Walls
    world.Add(std::make_shared<YZRect>(  0, 555,   0, 555, 555, green));
    world.Add(std::make_shared<YZRect>(  0, 555,   0, 555,   0, red));
    world.Add(std::make_shared<XZRect>(213, 343, 227, 332, 554, light));
    world.Add(std::make_shared<XZRect>(  0, 555,   0, 555,   0, white));
    world.Add(std::make_shared<XZRect>(  0, 555,   0, 555, 555, white));
    world.Add(std::make_shared<XYRect>(  0, 555,   0, 555, 555, white));

    // Boxes
    std::shared_ptr<Hittable> box1 = std::make_shared<Box>(Point3(0.0, 0.0, 0.0), Point3(165.0, 330.0, 165.0), white);
    box1 = std::make_shared<RotateY>(box1, 15.0);
    box1 = std::make_shared<Translate>(box1, Vec3(265.0, 0.0, 295.0));

    std::shared_ptr<Hittable> box2 = std::make_shared<Box>(Point3(0.0, 0.0, 0.0), Point3(165.0, 165.0, 165.0), white);
    box2 = std::make_shared<RotateY>(box2, -18.0);
    box2 = std::make_shared<Translate>(box2, Vec3(130.0, 0.0, 65.0));

    world.Add(std::make_shared<ConstantMedium>(box1, 0.01, Colour(0.0, 0.0, 0.0)));
    world.Add(std::make_shared<ConstantMedium>(box2, 0.01, Colour(1.0, 1.0, 1.0)));
  }

  static void Book2FinalScene(HittableList& world, CameraData& camData, Colour& background)
  {
    using namespace std;

    camData = GetDefaultCameraData();
    camData.position    = Point3(478, 278, -600);
    camData.lookAt      = Point3(278, 278, 0);
    camData.viewUp      = Vec3(0.0, 1.0, 0.0);
    camData.verticalFOV = 40.0;

    background = Colour(0.3, 0.3, 0.3); 

    HittableList boxes1;
    auto ground = make_shared<Lambertian>(Colour(0.48, 0.83, 0.53));

    const int boxes_per_side = 20;
    for (int i = 0; i < boxes_per_side; i++) 
    {
      for (int j = 0; j < boxes_per_side; j++) 
      {
        auto w = 100.0;
        auto x0 = -1000.0 + i*w;
        auto z0 = -1000.0 + j*w;
        auto y0 = 0.0;
        auto x1 = x0 + w;
        auto y1 = Random::Range<float64>(1.0, 101.0);
        auto z1 = z0 + w;

        boxes1.Add(make_shared<Box>(Point3(x0,y0,z0), Point3(x1, y1, z1), ground));
      }
    }

    world.Add(make_shared<BVHNode>(boxes1, 0, 1));

    auto light = make_shared<DiffuseLight>(Colour(7, 7, 7));
    world.Add(make_shared<XZRect>(123, 423, 147, 412, 554, light));

    auto center1 = Point3(400, 400, 200);
    auto center2 = center1 + Vec3(30,0,0);
    auto moving_sphere_material = make_shared<Lambertian>(Colour(0.7, 0.3, 0.1));
    world.Add(make_shared<MovingSphere>(center1, center2, 0, 1, 50, moving_sphere_material));

    world.Add(make_shared<Sphere>(Point3(260, 150, 45), 50, make_shared<Dielectric>(1.5)));
    world.Add(make_shared<Sphere>(Point3(0, 150, 145), 50, make_shared<Metal>(Colour(0.8, 0.8, 0.9), 1.0)));

    auto boundary = make_shared<Sphere>(Point3(360,150,145), 70, make_shared<Dielectric>(1.5));
    world.Add(boundary);
    world.Add(make_shared<ConstantMedium>(boundary, 0.2, Colour(0.2, 0.4, 0.9)));
    boundary = make_shared<Sphere>(Point3(0, 0, 0), 5000, make_shared<Dielectric>(1.5));
    world.Add(make_shared<ConstantMedium>(boundary, .0001, Colour(1,1,1)));

    auto emat = make_shared<Lambertian>(make_shared<ImageTexture>("./assets/earthmap.jpg"));
    world.Add(make_shared<Sphere>(Point3(400,200,400), 100, emat));
    auto pertext = make_shared<NoiseTexture>(0.1);
    world.Add(make_shared<Sphere>(Point3(220,280,300), 80, make_shared<Lambertian>(pertext)));

    HittableList boxes2;
    auto white = make_shared<Lambertian>(Colour(.73, .73, .73));
    int ns = 1000;
    for (int j = 0; j < ns; j++) 
    {
      boxes2.Add(make_shared<Sphere>(RandomVec3(0,165), 10, white));
    }

    world.Add(make_shared<Translate>(make_shared<RotateY>(make_shared<BVHNode>(boxes2, 0.0, 1.0), 15), Vec3(-100,270,395)));
  }

 private:
  inline static CameraData GetDefaultCameraData()
  {
    CameraData data;
    data.position    = Point3(13.0, 2.0, 3.0);
    data.lookAt      = Point3(0.0, 0.0, 0.0);
    data.viewUp      = Vec3(0.0, 1.0, 0.0);
    data.verticalFOV = 20.0;
    data.focusDist   = 10.0;
    data.aperture    = 0.1;
    data.startTime   = 0.0;
    data.endTime     = 1.0;

    return data;
  }
};

#endif // SCENES_HPP