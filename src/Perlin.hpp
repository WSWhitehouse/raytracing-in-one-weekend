#ifndef PERLIN_HPP
#define PERLIN_HPP

#include "Common.hpp"

class Perlin
{
 public: 
  Perlin()
  {
    randomVec = new Vec3[PointCount];

    for(int32 i = 0; i < PointCount; ++i)
    {
      randomVec[i] = UnitVector(RandomVec3(-1.0, 1.0));
    }

    permX = PerlinGeneratePerm();
    permY = PerlinGeneratePerm();
    permZ = PerlinGeneratePerm();
  }

  ~Perlin()
  {
    delete[] randomVec;
    delete[] permX;
    delete[] permY;
    delete[] permZ;
  }

  float64 Noise(const Point3& p) const
  {
    float64 u = p.x() - floor(p.x());
    float64 v = p.y() - floor(p.y());
    float64 w = p.z() - floor(p.z());

    int32 i = (int32)floor(p.x());
    int32 j = (int32)floor(p.y());
    int32 k = (int32)floor(p.z());
    Vec3 c[2][2][2];

    for(int32 di = 0; di < 2; di++)
    {
      for(int32 dj = 0; dj < 2; dj++)
      {
        for(int32 dk = 0; dk < 2; dk++)
        {
          c[di][dj][dk] = randomVec[
              permX[(i + di) & 255] ^
              permY[(j + dj) & 255] ^
              permZ[(k + dk) & 255]
            ];
        }
      }
    }

    return TrilinearInterp(c, u, v, w);
  }

  // NOTE(WSWhitehouse): Composite noise that has multiple summed frequencies - a sum of repeated calls to noise
  float64 Turbulence(const Point3& p, int32 depth = 7) const
  {
    float64 accum  = 0.0;
    Point3 tempP   = p;
    float64 weight = 1.0;

    for(int32 i = 0; i < depth; i++)
    {
      accum  += weight * Noise(tempP);
      weight *= 0.5;
      tempP  *= 2.0;
    }

    return fabs(accum);
  }

 private:
  static const int PointCount = 256;

  Vec3* randomVec;
  int32* permX;
  int32* permY;
  int32* permZ;

  static int32* PerlinGeneratePerm()
  {
    auto p = new int32[PointCount];

    for (int32 i = 0; i < PointCount; i++)
    {
      p[i] = i;
    }

    Permute(p, PointCount);
    return p;
  }

  static void Permute(int32* p, int32 n)
  {
    for (int32 i = n - 1; i > 0; i--)
    {
      int32 target = Random::Range<int32>(0, i);
      int32 temp   = p[i];

      p[i]      = p[target];
      p[target] = temp;
    }
  }

  static float64 TrilinearInterp(Vec3 c[2][2][2], float64 u, float64 v, float64 w)
  {
    float64 uu = u * u * (3 - 2 * u);
    float64 vv = v * v * (3 - 2 * v);
    float64 ww = w * w * (3 - 2 * w);

    float64 accum = 0.0;

    for(int32 i = 0; i < 2; i++)
    {
      for(int32 j = 0; j < 2; j++)
      {
        for(int32 k = 0; k < 2; k++)
        {
          Vec3 weightV(u - i, v - j, w - k);
          accum += (i * uu + (1 - i) * (1 - uu)) *
                   (j * vv + (1 - j) * (1 - vv)) *
                   (k * ww + (1 - k) * (1 - ww)) *
                   Dot(c[i][j][k], weightV);
        }
      }
    }

    return accum;
  }
};


#endif // PERLIN_HPP