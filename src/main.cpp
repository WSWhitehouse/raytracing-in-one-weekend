#include <iostream>

#include "Common.hpp"
#include "Colour.hpp"
#include "Camera.hpp"
#include "Random.hpp"
#include "HitRecord.hpp"
#include "Scenes.hpp"
#include "ImageData.hpp"

// Hittable
#include "hittable/HittableList.hpp"
#include "hittable/Sphere.hpp"
#include "hittable/BVH.hpp"

// Materials
#include "material/Material.hpp"
#include "material/Lambertian.hpp"
#include "material/Metal.hpp"
#include "material/Dielectric.hpp"

Colour RayColour(const Ray& ray, const Colour& background, const Hittable& world, int32 depth)
{
  HitRecord record;

  if (depth <= 0) return Colour(0.0, 0.0, 0.0);

  if (!world.Hit(ray, 0.001, Infinity64, record)) return background;

  Ray scattered;
  Colour attenuation;
  Colour emitted = record.materialPtr->Emitted(record.u, record.v, record.p);

  if (!record.materialPtr->Scatter(ray, record, attenuation, scattered)) return emitted;
  return emitted + attenuation * RayColour(scattered, background, world, depth - 1);
}

int main()
{
  ImageData imageData = ImageData::DefaultImageData(AspectRatio1x1);
  CameraData cameraData {};
  HittableList world    {};
  Colour background     {};

  // Scenes::Default(world, cameraData, background);
  // Scenes::RandomSpheres(world, cameraData, background);
  // Scenes::RandomMovingSpheres(world, cameraData, background);
  // Scenes::TwoSpheres(world, cameraData, background);
  // Scenes::Earth(world, cameraData, background);
  // Scenes::SimpleLight(world, cameraData, background);
  // Scenes::CornellBox(world, cameraData, background);
  // Scenes::CornellSmoke(world, cameraData, background);
  Scenes::Book2FinalScene(world, cameraData, background);

  Camera cam(cameraData, imageData);

  // Render
  std::cout << "P3\n" << imageData.width << ' ' << imageData.height << "\n255\n";
  
  for (int32 j = imageData.height - 1; j >= 0; --j)
  {
    std::cerr << "\rScanlines Remaining: " << j << " / " << imageData.height << ' ' << std::flush;
    for (int32 i = 0; i < imageData.width; ++i)
    {
      Colour pixelColour(0, 0, 0);

      for (int32 s = 0; s < imageData.samplesPerPixel; ++s)
      {
        float64 u = (i + Random::Range()) / (imageData.width - 1);
        float64 v = (j + Random::Range()) / (imageData.height - 1);

        Ray ray      = cam.GetRay(u, v);
        pixelColour += RayColour(ray, background, world, imageData.maxDepth);
      }

      WriteColour(std::cout, pixelColour, imageData.samplesPerPixel);
    }
  }

  std::cerr << "\nDone!\n";
}