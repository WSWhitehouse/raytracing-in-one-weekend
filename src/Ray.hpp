#ifndef RAY_HPP
#define RAY_HPP

#include "Vec3.hpp"

struct Ray
{
  Ray() {}
  Ray(const Point3& origin, const Vec3& direction, float64 time = 0.0) 
    : orig(origin), dir(direction), tm(time) {}

  Point3 Origin() const  { return orig; }
  Vec3 Direction() const { return dir; }
  float64 Time() const   { return tm; }

  Point3 At(float64 t) const
  {
    return orig + t * dir;
  }

 private:
  Point3 orig;
  Vec3 dir;
  float64 tm;
};

#endif // RAY_HPP