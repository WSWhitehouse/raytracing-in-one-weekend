#ifndef IMAGE_DATA_HPP
#define IMAGE_DATA_HPP

#include "Common.hpp"

const inline static float64 AspectRatio16x9 = 16.0 / 9.0;
const inline static float64 AspectRatio1x1  = 1.0;

struct ImageData
{
  float64 aspectRatio;
  int32 width;
  int32 height;
  int32 samplesPerPixel;
  int32 maxDepth; 

  inline static ImageData DefaultImageData(float64 aspectRatio = AspectRatio16x9)
  {
    ImageData data;
    data.aspectRatio     = aspectRatio;
    data.width           = 400;
    data.height          = (int32)(data.width / data.aspectRatio);
    data.samplesPerPixel = 100;
    data.maxDepth        = 50;
    return data;
  }

  // NOTE(WSWhitehouse): Use this image data in high res images, takes a while to generate
  inline static ImageData HighResImageData(float64 aspectRatio = AspectRatio16x9)
  {
    ImageData data;
    data.aspectRatio     = aspectRatio;
    data.width           = 1200;
    data.height          = (int32)(data.width / data.aspectRatio);
    data.samplesPerPixel = 500;
    data.maxDepth        = 50;
    return data;
  }

  inline static ImageData MedResImageData(float64 aspectRatio = AspectRatio16x9)
  {
    ImageData data;
    data.aspectRatio     = aspectRatio;
    data.width           = 400;
    data.height          = (int32)(data.width / data.aspectRatio);
    data.samplesPerPixel = 400;
    data.maxDepth        = 50;
    return data;
  }

  // NOTE(WSWhitehouse): Use this image data in low res or test images, doesn't take too long to generate
  inline static ImageData LowResImageData(float64 aspectRatio = AspectRatio16x9)
  {
    ImageData data;
    data.aspectRatio     = aspectRatio;
    data.width           = 400;
    data.height          = (int32)(data.width / data.aspectRatio);
    data.samplesPerPixel = 25;
    data.maxDepth        = 25;
    return data;
  }
};

#endif // IMAGE_DATA_HPP