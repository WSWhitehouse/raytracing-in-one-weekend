#ifndef CHECKER_TEXTURE_HPP
#define CHECKER_TEXTURE_HPP

#include "../Common.hpp"
#include "Texture.hpp"
#include "SolidColour.hpp"

struct CheckerTexture : public Texture
{
  CheckerTexture() { }
  CheckerTexture(std::shared_ptr<Texture> even, std::shared_ptr<Texture> odd) 
    : even(even), odd(odd) { }
  CheckerTexture(Colour evenCol, Colour oddCol) 
    : even(std::make_shared<SolidColour>(evenCol)), odd(std::make_shared<SolidColour>(oddCol)) { }

  virtual ~CheckerTexture() override = default;

  virtual Colour Value(float64 u, float64 v, const Point3& p) const override
  {
    auto sines = sin(10 * p.x()) * sin(10 * p.y()) * sin(10 * p.z());

    if (sines < 0) return odd->Value(u, v, p);
    else           return even->Value(u, v, p);
  }

  std::shared_ptr<Texture> even;
  std::shared_ptr<Texture> odd;
};

#endif // CHECKER_TEXTURE_HPP