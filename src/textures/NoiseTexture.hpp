#ifndef NOISE_TEXTURE_HPP
#define NOISE_TEXTURE_HPP

#include "../Common.hpp"
#include "../Perlin.hpp"
#include "Texture.hpp"

struct NoiseTexture : public Texture
{
  NoiseTexture() { }
  NoiseTexture(float64 frequency) : frequency(frequency) { }

  virtual ~NoiseTexture() override = default;

  virtual Colour Value(float64 u, float64 v, const Point3& p) const override
  { 
    // Normal Perlin Noise
    // NOTE(WSWhitehouse): Casting perlin output back between 0 and 1 to prevent NaNs
    // return Colour(1, 1, 1) * 0.5 * (1.0 + noise.Noise(frequency * p)); 

    // Turbulence
    // return Colour(1, 1, 1) * noise.Turbulence(frequency * p);  

    // "Marble-Like" Texture using Noise
    return Colour(1, 1, 1) * 0.5 * (1 - sin(frequency * p.z() + 10 * noise.Turbulence(p)));
  }

  Perlin noise;
  float64 frequency = 1.0;
};

#endif // NOISE_TEXTURE_HPP