#ifndef IMAGE_TEXTURE_HPP
#define IMAGE_TEXTURE_HPP

#include "Common.hpp"
#include "Perlin.hpp"
#include "Texture.hpp"
#include "STBImageWrapper.hpp"
#include <iostream>

struct ImageTexture : public Texture
{
  const static int32 BytesPerPixel = 3;

  ImageTexture() { }
  ImageTexture(const char* filePath)
  {
    int32 componentsPerPixel = BytesPerPixel;

    data = stbi_load(filePath, &width, &height, &componentsPerPixel, componentsPerPixel);

    if (!data)
    {
      std::cerr << "ERROR: Could not load texture image file '" << filePath << "'.\n";
      width  = 0;
      height = 0;
    }

    bytesPerScanline = BytesPerPixel * width;
  }

  virtual ~ImageTexture() override
  {
    delete data;
  }

  virtual Colour Value(float64 u, float64 v, const Point3& p) const override
  {
    // NOTE(WSWhitehouse): If texture data is nullptr return a solid cyan colour for debugging
    if (data == nullptr) return Colour(0.0, 1.0, 0.0);

    // Clamp input texture coordinates to [0,1] x [1,0]
    u = clamp(u, 0.0, 1.0);
    v = 1.0 - clamp(v, 0.0, 1.0); // Flip v coordinate

    int32 i = (int32)(u * width);
    int32 j = (int32)(v * height);

    // Clamp integer mapping, since actual coordinates should be less than 1.0
    if (i >= width)  i = width - 1;
    if (j >= height) j = height - 1;

    const float64 colourScale = 1.0 / 255.0;
    auto pixel = data + j * bytesPerScanline + i * BytesPerPixel;
    return Colour(colourScale * pixel[0], colourScale * pixel[1], colourScale * pixel[2]); 
  }

 private:
  uchar *data = nullptr;
  int32 width, height;
  int32 bytesPerScanline;
};

#endif // IMAGE_TEXTURE_HPP