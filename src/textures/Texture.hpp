#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include "../Common.hpp"

struct Texture
{
  Texture()          = default;
  virtual ~Texture() = default;

  virtual Colour Value(float64 u, float64 v, const Point3& p) const = 0;
};

#endif // TEXTURE_HPP