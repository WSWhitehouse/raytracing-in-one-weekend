#ifndef SOLID_COLOUR_HPP
#define SOLID_COLOUR_HPP

#include "../Common.hpp"
#include "Texture.hpp"

struct SolidColour : public Texture
{
  SolidColour() { }
  SolidColour(Colour col) : ColourVal(col) { }
  SolidColour(float64 r, float64 g, float64 b) : ColourVal(Colour(r, g, b)) { }

  virtual ~SolidColour() override = default;

  virtual Colour Value(float64 u, float64 v, const Point3& p) const override { return ColourVal; }

 private:
  Colour ColourVal;
};

#endif // SOLID_COLOUR_HPP