#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstdint>
#include <cmath>
#include <limits>
#include <memory>

// ---------- //
//  Typedefs  //
// ---------- //

// Integer
typedef int8_t  int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

// Boolean
typedef int32 bool32;

// Char
typedef unsigned char uchar;

// Byte
typedef uchar byte;
typedef char  sbyte;

// Floating Point
typedef float  float32;
typedef double float64;

// Include Common Types
#include "Vec3.hpp"
#include "Ray.hpp"  

// ----------- //
//  Constants  //
// ----------- //

const float32 Infinity32 = std::numeric_limits<float32>::infinity();
const float64 Infinity64 = std::numeric_limits<float64>::infinity();

// --------- //
//  Defines  //
// --------- //

#define ARRAY_SIZE(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))


// TODO(WSWhitehouse): Move code below to math file
const float64 pi = 3.1415926535897932385;

inline float64 DegreesToRadians(float64 degrees)
{
  return degrees * pi / 180;
}

inline float64 clamp(float64 val, float64 min, float64 max)
{
  if (val < min) return min;
  if (val > max) return max;
  return val;
}

#endif // COMMON_HPP