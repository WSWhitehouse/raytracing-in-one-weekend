#ifndef ROTATE_HPP
#define ROTATE_HPP

#include "Common.hpp"
#include "hittable/Hittable.hpp"

struct RotateY : public Hittable
{
  RotateY(std::shared_ptr<Hittable> hittable, float64 angle) : hittable(hittable)
  {
    float64 radians = DegreesToRadians(angle);

    sinTheta = sin(radians);
    cosTheta = cos(radians);
    hasBox   = hittable->BoundingBox(0, 1, boundingBox);

    Point3 min( Infinity64,  Infinity64,  Infinity64);
    Point3 max(-Infinity64, -Infinity64, -Infinity64);

    for (int32 i = 0; i < 2; i++)
    {
      for (int32 j = 0; j < 2; j++)
      {
        for (int32 k = 0; k < 2; k++)
        {
          float64 x = i * boundingBox.maximum.x() + (1 - i) * boundingBox.minimum.x();
          float64 y = j * boundingBox.maximum.y() + (1 - j) * boundingBox.minimum.y();
          float64 z = k * boundingBox.maximum.z() + (1 - k) * boundingBox.minimum.z();

          float64 newX =  cosTheta * x + sinTheta * z;
          float64 newZ = -sinTheta * x + cosTheta * z; 

          Vec3 tester(newX, y, newZ);

          for (int32 c = 0; c < 3; c++)
          {
            min[c] = fmin(min[c], tester[c]);
            max[c] = fmax(max[c], tester[c]);
          }
        }         
      }
    }
    
    boundingBox = AABB(min, max);
  }

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override
  {
    Vec3 origin    = ray.Origin();
    Vec3 direction = ray.Direction();

    origin[0] = cosTheta * ray.Origin()[0] - sinTheta * ray.Origin()[2];
    origin[2] = sinTheta * ray.Origin()[0] + cosTheta * ray.Origin()[2];

    direction[0] = cosTheta * ray.Direction()[0] - sinTheta * ray.Direction()[2];
    direction[2] = sinTheta * ray.Direction()[0] + cosTheta * ray.Direction()[2];

    Ray rotatedRay(origin, direction, ray.Time());
    if (!hittable->Hit(rotatedRay, tMin, tMax, record)) return false;

    Point3 p    = record.p;
    Vec3 normal = record.normal;

    p[0] =  cosTheta * record.p[0] + sinTheta * record.p[2];
    p[2] = -sinTheta * record.p[0] + cosTheta * record.p[2];

    normal[0] =  cosTheta * record.normal[0] + sinTheta * record.normal[2];
    normal[2] = -sinTheta * record.normal[0] + cosTheta * record.normal[2];

    record.p = p;
    record.SetFaceNormal(rotatedRay, normal);
    return true;
  }

  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override
  {
    outputAABB = boundingBox;
    return hasBox;
  }

  std::shared_ptr<Hittable> hittable;
  float64 sinTheta;
  float64 cosTheta;
  bool32 hasBox;
  AABB boundingBox;
};

#endif // ROTATE_HPP