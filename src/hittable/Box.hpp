#ifndef BOX_HPP
#define BOX_HPP

#include "Common.hpp"
#include "hittable/Hittable.hpp"
#include "hittable/HittableList.hpp"
#include "hittable/AARect.hpp"

struct Box : public Hittable
{
  Box(const Point3& min, const Point3& max, std::shared_ptr<Material> mat)
  {
    boxMin = min;
    boxMax = max;

    sides.Add(std::make_shared<XYRect>(min.x(), max.x(), min.y(), max.y(), max.z(), mat));
    sides.Add(std::make_shared<XYRect>(min.x(), max.x(), min.y(), max.y(), min.z(), mat));

    sides.Add(std::make_shared<XZRect>(min.x(), max.x(), min.z(), max.z(), max.y(), mat));
    sides.Add(std::make_shared<XZRect>(min.x(), max.x(), min.z(), max.z(), min.y(), mat));

    sides.Add(std::make_shared<YZRect>(min.y(), max.y(), min.z(), max.z(), max.x(), mat));
    sides.Add(std::make_shared<YZRect>(min.y(), max.y(), min.z(), max.z(), min.x(), mat));
  }

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override
  {
    return sides.Hit(ray, tMin, tMax, record);
  }

  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override
  {
    outputAABB = AABB(boxMin, boxMax);
    return true;
  }

  Point3 boxMin;
  Point3 boxMax;
  HittableList sides;
};

#endif // BOX_HPP