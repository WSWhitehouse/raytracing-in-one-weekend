#ifndef HITTABLE_HPP
#define HITTABLE_HPP

#include "Common.hpp"
#include "HitRecord.hpp"
#include "AABB.hpp"

struct Hittable
{
  virtual ~Hittable() = default;

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const = 0;
  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const        = 0;
};

#endif // HITTABLE_HPP