#ifndef AARECT_HPP
#define AARECT_HPP

#include "Common.hpp"
#include "Hittable.hpp"
#include "material/Material.hpp"

struct XYRect : public Hittable
{
  XYRect() = default;
  XYRect(float64 x0, float64 x1, float64 y0, float64 y1, float64 k, std::shared_ptr<Material> mat)
    : x0(x0), x1(x1), y0(y0), y1(y1), k(k), mat(mat) { }

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override
  {
    float64 t = (k - ray.Origin().z()) / ray.Direction().z();
    if (t < tMin || t > tMax) return false;

    float64 x = ray.Origin().x() + t * ray.Direction().x();
    float64 y = ray.Origin().y() + t * ray.Direction().y();
    if (x < x0 || x > x1 || y < y0 || y > y1) return false;

    record.u = (x - x0) / (x1 - x0);
    record.v = (y - y0) / (y1 - y0);
    record.t = t;
    record.p = ray.At(t);

    Vec3 outwardNormal = Vec3(0.0, 0.0, 1.0);
    record.SetFaceNormal(ray, outwardNormal);
    record.materialPtr = mat;
    
    return true;
  }

  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override
  {
    // The bounding box must have non-zero width in each dimension, so pad the Z dimension a small amount.
    outputAABB = AABB(Point3(x0, y0, k - 0.0001), Point3(x1, y1, k + 0.0001));
    return true;
  }

  float64 x0, x1, y0, y1;
  float64 k;
  std::shared_ptr<Material> mat;
};

struct XZRect : public Hittable
{
  XZRect() = default;
  XZRect(float64 x0, float64 x1, float64 z0, float64 z1, float64 k, std::shared_ptr<Material> mat)
    : x0(x0), x1(x1), z0(z0), z1(z1), k(k), mat(mat) { }

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override
  {
    float64 t = (k - ray.Origin().y()) / ray.Direction().y();
    if (t < tMin || t > tMax) return false;

    float64 x = ray.Origin().x() + t * ray.Direction().x();
    float64 z = ray.Origin().z() + t * ray.Direction().z();
    if (x < x0 || x > x1 || z < z0 || z > z1) return false;

    record.u = (x - x0) / (x1 - x0);
    record.v = (z - z0) / (z1 - z0);
    record.t = t;
    record.p = ray.At(t);

    Vec3 outwardNormal = Vec3(0.0, 1.0, 0.0);
    record.SetFaceNormal(ray, outwardNormal);
    record.materialPtr = mat;

    return true;
  }

  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override
  {
    // The bounding box must have non-zero width in each dimension, so pad the Y dimension a small amount.
    outputAABB = AABB(Point3(x0, k - 0.0001, z0), Point3(x1, k + 0.0001, z1));
    return true;
  }

  float64 x0, x1, z0, z1;
  float64 k;
  std::shared_ptr<Material> mat;
};

struct YZRect : public Hittable
{
  YZRect() = default;
  YZRect(float64 y0, float64 y1, float64 z0, float64 z1, float64 k, std::shared_ptr<Material> mat)
    : y0(y0), y1(y1), z0(z0), z1(z1), k(k), mat(mat) {};

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override
  {
    float64 t = (k - ray.Origin().x()) / ray.Direction().x();
    if (t < tMin || t > tMax) return false;

    float64 y = ray.Origin().y() + t * ray.Direction().y();
    float64 z = ray.Origin().z() + t * ray.Direction().z();
    if (y < y0 || y > y1 || z < z0 || z > z1) return false;

    record.u = (y - y0) / (y1 - y0);
    record.v = (z - z0) / (z1 - z0);
    record.t = t;
    record.p = ray.At(t);

    Vec3 outwardNormal = Vec3(1.0, 0.0, 0.0);
    record.SetFaceNormal(ray, outwardNormal);
    record.materialPtr = mat;

    return true;
  }

  virtual bool32 BoundingBox(float64 time0,float64 time1, AABB& output_box) const override 
  {
    // The bounding box must have non-zero width in each dimension, so pad the X dimension a small amount.
    output_box = AABB(Point3(k - 0.0001, y0, z0), Point3(k + 0.0001, y1, z1));
    return true;
  }

  float64 y0, y1, z0, z1;
  float64 k;
  std::shared_ptr<Material> mat;
};

#endif // AARECT_HPP