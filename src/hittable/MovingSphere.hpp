#ifndef MOVING_SPHERE_HPP
#define MOVING_SPHERE_HPP

#include "../Common.hpp"
#include "Hittable.hpp"

struct MovingSphere : public Hittable
{
  MovingSphere() { }
  MovingSphere(Point3 cen0, Point3 cen1, float64 _time0, float64 _time1, float64 radius, std::shared_ptr<Material> mat)
   : center0(cen0), center1(cen1), time0(_time0), time1(_time1), radius(radius), material(mat) 
   { }

  virtual ~MovingSphere() override = default;

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override;
  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override;

  inline Point3 Center(float64 time) const { return center0 + ((time - time0) / (time1 - time0)) * (center1 - center0); }

  Point3 center0, center1;
  float64 time0, time1;
  float64 radius;
  std::shared_ptr<Material> material;
};

bool32 MovingSphere::Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const
{
  Point3 center  = Center(ray.Time());
  Vec3 oc        = ray.Origin() - center;
  float64 a      = ray.Direction().LengthSquared();
  float64 half_b = Dot(oc, ray.Direction());
  float64 c      = oc.LengthSquared() - (radius * radius);

  float64 discriminant = (half_b * half_b) - (a * c);
  if (discriminant < 0) return false;

  float64 sqrt_d = sqrt(discriminant);

  float64 root = (-half_b - sqrt_d) / a;
  if (root < tMin || root > tMax)
  {
    root = (-half_b + sqrt_d) / a;
    if (root < tMin || root > tMax) return false;
  }

  // Set record data
  record.t = root;
  record.p = ray.At(root);
  record.materialPtr = material;

  Vec3 outwardNormal = (record.p - center) / radius;
  record.SetFaceNormal(ray, outwardNormal);

  return true;
}

bool32 MovingSphere::BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const 
{
  Point3 center0 = Center(time0);
  Point3 center1 = Center(time1);

  AABB box0(
    center0 - Vec3(radius, radius, radius),
    center0 + Vec3(radius, radius, radius)
  );

  AABB box1(
    center1 - Vec3(radius, radius, radius),
    center1 + Vec3(radius, radius, radius)
  );

  outputAABB = AABB::SurrondingBox(box0, box1);
  return true;
}

#endif // MOVING_SPHERE_HPP