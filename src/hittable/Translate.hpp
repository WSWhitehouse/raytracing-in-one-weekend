#ifndef TRANSLATE_HPP
#define TRANSLATE_HPP

#include "Common.hpp"
#include "hittable/Hittable.hpp"

struct Translate : public Hittable
{
  Translate(std::shared_ptr<Hittable> hittable, const Vec3& displacement)
    : hittable(hittable), displacement(displacement) { }

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override
  {
    Ray movedRay(ray.Origin() - displacement, ray.Direction(), ray.Time());
    if (!hittable->Hit(movedRay, tMin, tMax, record)) return false;

    record.p += displacement;
    record.SetFaceNormal(movedRay, record.normal);
    return true;
  }

  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override
  {
    if (!hittable->BoundingBox(time0, time1, outputAABB)) return false;

    outputAABB = AABB(
      outputAABB.minimum + displacement,
      outputAABB.maximum + displacement
    );

    return true;
  }

  std::shared_ptr<Hittable> hittable;
  Vec3 displacement;
};

#endif // TRANSLATE_HPP