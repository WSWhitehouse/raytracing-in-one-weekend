#ifndef BVH_HPP
#define BVH_HPP

// BVH = Bounding Volume Hierarchy

#include "../Common.hpp"
#include "Hittable.hpp"
#include "HittableList.hpp"
#include <algorithm>

struct BVHNode : public Hittable
{
  BVHNode();
  BVHNode(const HittableList& list, float64 time0, float64 time1) : BVHNode(list.objects, 0, list.objects.size(), time0, time1) { }
  BVHNode(const std::vector<std::shared_ptr<Hittable>>& srcObjects, size_t start, size_t end, float64 time0, float64 time1);

  virtual ~BVHNode() override = default;

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override;
  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override { outputAABB = box; return true; }

  std::shared_ptr<Hittable> left;
  std::shared_ptr<Hittable> right;
  AABB box;
};

bool32 BVHNode::Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const 
{
  if (!box.Hit(ray, tMin, tMax)) return false;

  bool32 hitLeft  = left->Hit(ray, tMin, tMax, record);
  bool32 hitRight = right->Hit(ray, tMin, hitLeft ? record.t : tMax, record);

  return hitLeft || hitRight;
}

inline bool32 BoxCompare(const std::shared_ptr<Hittable> a, const std::shared_ptr<Hittable> b, int axis)
{
  AABB boxA;
  AABB boxB;

  if (!a->BoundingBox(0, 0, boxA) || !b->BoundingBox(0, 0, boxB))
  {
    std::cerr << "No bounding box in BVH Node constructor. \n";
  }

  return boxA.minimum.e[axis] < boxB.minimum.e[axis];
}

bool32 BoxXCompare(const std::shared_ptr<Hittable> a, const std::shared_ptr<Hittable> b) { return BoxCompare(a, b, 0); }
bool32 BoxYCompare(const std::shared_ptr<Hittable> a, const std::shared_ptr<Hittable> b) { return BoxCompare(a, b, 1); }
bool32 BoxZCompare(const std::shared_ptr<Hittable> a, const std::shared_ptr<Hittable> b) { return BoxCompare(a, b, 2); }

BVHNode::BVHNode(const std::vector<std::shared_ptr<Hittable>>& srcObjects, size_t start, size_t end, float64 time0, float64 time1)
{
  auto objects = srcObjects;
  int32 axis   = Random::Range<int32>(0, 2);

  auto comparator = (axis == 0) ? BoxXCompare
                  : (axis == 1) ? BoxYCompare
                                : BoxZCompare;

  size_t objectSpan = end - start;
  if (objectSpan == 1)
  {
    left = right = objects[start];
  }
  else if (objectSpan == 2)
  {
    if (comparator(objects[start], objects[start + 1]))
    {
      left  = objects[start];
      right = objects[start + 1];
    }
    else
    {
      left  = objects[start + 1];
      right = objects[start];
    }
  }
  else
  {
    std::sort(objects.begin() + start, objects.begin() + end, comparator);

    auto mid = start + (objectSpan * 0.5);
    left     = std::make_shared<BVHNode>(objects, start, mid, time0, time1);
    right    = std::make_shared<BVHNode>(objects, mid, end, time0, time1);
  }

  AABB boxLeft, boxRight;

  if (!left->BoundingBox(time0, time1, boxLeft) ||
      !right->BoundingBox(time0, time1, boxRight))
  {
    std::cerr << "No bounding box in BVH Node constructor.\n";
  }

  box = AABB::SurrondingBox(boxLeft, boxRight);
}

#endif // BVH_HPP