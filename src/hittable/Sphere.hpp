#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "Hittable.hpp"
#include "../Common.hpp"

struct Sphere : public Hittable
{
  Sphere() {}
  Sphere(Point3 cen, double rad, std::shared_ptr<Material> mat) :
   center(cen), radius(rad), material(mat) {}

  virtual ~Sphere() override = default;

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override;
  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override;

  static void GetSphereUV(const Point3& p, float64& u, float64& v)
  {
    // NOTE(WSWhitehouse):
    // p: a given point on the sphere of radius one, centered at the origin.
    // u: returned value [0,1] of angle around the Y axis from X=-1.
    // v: returned value [0,1] of angle from Y=-1 to Y=+1.
    //     <1 0 0> yields <0.50 0.50>       <-1  0  0> yields <0.00 0.50>
    //     <0 1 0> yields <0.50 1.00>       < 0 -1  0> yields <0.50 0.00>
    //     <0 0 1> yields <0.25 0.50>       < 0  0 -1> yields <0.75 0.50>

    float64 theta = acos(-p.y());
    float64 phi   = atan2(-p.z(), p.x()) + pi;

    u = phi / (2 * pi);
    v = theta / pi;
  }

  Point3 center;
  double radius;
  std::shared_ptr<Material> material;
};

bool32 Sphere::Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const
{
  Vec3 oc        = ray.Origin() - center;
  float64 a      = ray.Direction().LengthSquared();
  float64 half_b = Dot(oc, ray.Direction());
  float64 c      = oc.LengthSquared() - (radius * radius);

  float64 discriminant = (half_b * half_b) - (a * c);
  if (discriminant < 0) return false;

  float64 sqrt_d = sqrt(discriminant);

  float64 root = (-half_b - sqrt_d) / a;
  if (root < tMin || root > tMax)
  {
    root = (-half_b + sqrt_d) / a;
    if (root < tMin || root > tMax) return false;
  }

  // Set record data
  record.t           = root;
  record.p           = ray.At(root);
  record.materialPtr = material;
  
  Vec3 outwardNormal = (record.p - center) / radius;
  record.SetFaceNormal(ray, outwardNormal);
  GetSphereUV(outwardNormal, record.u, record.v);

  return true;
}

bool32 Sphere::BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const
{
  outputAABB = AABB(
    center - Vec3(radius, radius, radius),
    center + Vec3(radius, radius, radius)
  );

  return true;
}

#endif // SPHERE_HPP