#ifndef HITTABLE_LIST_HPP
#define HITTABLE_LIST_HPP

#include "Hittable.hpp"

#include <memory>
#include <vector>

struct HittableList : public Hittable
{
  HittableList() {}
  HittableList(std::shared_ptr<Hittable> object) { Add(object); }

  virtual ~HittableList() override = default;

  void Clear() { objects.clear(); }
  void Add(std::shared_ptr<Hittable> object) { objects.push_back(object); }

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override;
  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override;

  std::vector<std::shared_ptr<Hittable>> objects = {};
};

bool32 HittableList::Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const
{
  HitRecord tempRecord;
  bool32 hitAnything = false;
  float64 closestHit = tMax;

  for (const auto& object : objects)
  {
    if (object->Hit(ray, tMin, closestHit, tempRecord))
    {
      hitAnything = true;
      closestHit  = tempRecord.t;
      record      = tempRecord;
    }
  }

  return hitAnything;
}

bool32 HittableList::BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const
{
  if (objects.empty()) return false;

  AABB tempBox;
  bool32 firstBox = true;

  for (const auto& object : objects)
  {
    if (!object->BoundingBox(time0, time1, tempBox)) return false;
    outputAABB = firstBox ? tempBox : AABB::SurrondingBox(outputAABB, tempBox);
    firstBox   = false;
  }

  return true;
}

#endif // HITTABLE_LIST_HPP