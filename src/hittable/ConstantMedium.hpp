#ifndef CONSTANT_MEDIUM_HPP
#define CONSTANT_MEDIUM_HPP

#include "Common.hpp"
#include "hittable/Hittable.hpp"
#include "textures/Texture.hpp"
#include "material/Material.hpp"
#include "material/Isotropic.hpp"

struct ConstantMedium : public Hittable
{
  ConstantMedium(std::shared_ptr<Hittable> boundry, float64 negInvDensity, std::shared_ptr<Texture> tex)
    : boundry(boundry), phaseFunction(std::make_shared<Isotropic>(tex)), negInvDensity(-1 / negInvDensity) { }

  ConstantMedium(std::shared_ptr<Hittable> boundry, float64 negInvDensity, Colour col)
    : boundry(boundry), phaseFunction(std::make_shared<Isotropic>(col)), negInvDensity(-1 / negInvDensity) { }

  virtual bool32 Hit(const Ray& ray, float64 tMin, float64 tMax, HitRecord& record) const override
  {
    // NOTE(WSWhitehouse): Only enable debugging when boolean is set to true and on occasional passes thanks to random range
    const bool EnableDebug = false;
    const bool Debugging   = EnableDebug && Random::Range() < 0.00001;

    HitRecord rec1, rec2;

    if (!boundry->Hit(ray, -Infinity64, Infinity64, rec1))     return false;
    if (!boundry->Hit(ray, rec1.t + 0.0001, Infinity64, rec2)) return false;

    if (Debugging) std::cerr << "\n tMin = " << rec1.t << ", tMax = " << rec2.t << '\n';

    if (rec1.t < tMin) rec1.t = tMin;
    if (rec2.t > tMax) rec2.t = tMax;

    if (rec1.t >= rec2.t) return false;
    
    if (rec1.t < 0.0) rec1.t = 0.0;

    const auto rayLength         = ray.Direction().Length();
    const auto distInsideBoundry = (rec2.t - rec1.t) * rayLength;
    const auto hitDistance       = negInvDensity * log(Random::Range());

    if (hitDistance > distInsideBoundry) return false;

    record.t = rec1.t + hitDistance / rayLength;
    record.p = ray.At(record.t);

    if (Debugging)
    {
      std::cerr << "hit_distance = " <<  hitDistance << '\n'
                << "record.t     = " <<  record.t    << '\n'
                << "record.p     = " <<  record.p    << '\n';
    }

    record.normal      = Vec3(1.0, 0.0, 0.0);
    record.frontFace   = true;
    record.materialPtr = phaseFunction;
    return true;
  }

  virtual bool32 BoundingBox(float64 time0, float64 time1, AABB& outputAABB) const override
  {
    return boundry->BoundingBox(time0, time1, outputAABB);
  }

  std::shared_ptr<Hittable> boundry;
  std::shared_ptr<Material> phaseFunction;
  float64 negInvDensity;
};

#endif // CONSTANT_MEDIUM_HPP