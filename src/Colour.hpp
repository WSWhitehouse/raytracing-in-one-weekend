#ifndef COLOUR_HPP
#define COLOUR_HPP

#include "Common.hpp"
#include <iostream>

void WriteColour(std::ostream &out, Colour pixelColour, int32 samplesPerPixel) 
{
  float64 r = pixelColour.x();
  float64 g = pixelColour.y();
  float64 b = pixelColour.z();

  float64 scale = 1.0 / samplesPerPixel;
  r = sqrt(scale * r);
  g = sqrt(scale * g);
  b = sqrt(scale * b);

  // Write the translated [0,255] value of each color component
  out << (int32)(255.999 * clamp(r, 0.0, 0.999)) << ' '
      << (int32)(255.999 * clamp(g, 0.0, 0.999)) << ' '
      << (int32)(255.999 * clamp(b, 0.0, 0.999)) << '\n';
}

#endif // COLOUR_HPP