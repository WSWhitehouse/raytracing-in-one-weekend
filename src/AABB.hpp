#ifndef AABB_HPP
#define AABB_HPP

#include "Common.hpp"
#include <utility>

struct AABB
{
  AABB() { }
  AABB(const Point3& min, const Point3& max)
    : minimum(min), maximum(max) 
    { }

  inline bool32 Hit(const Ray& ray, float64 tMin, float64 tMax) const
  {
    for (int i = 0; i < 3; i++) 
    {
      float64 invD = 1.0 / ray.Direction()[i];
      float64 t0   = (minimum[i] - ray.Origin()[i]) * invD;
      float64 t1   = (maximum[i] - ray.Origin()[i]) * invD;
      
      if (invD < 0.0f) std::swap(t0, t1);

      tMin = t0 > tMin ? t0 : tMin;
      tMax = t1 < tMax ? t1 : tMax;
      
      if (tMax <= tMin) return false;
    }

    return true;
  }

  inline static AABB SurrondingBox(AABB box0, AABB box1)
  {
    Point3 min(
      fmin(box0.minimum.x(), box1.minimum.x()),
      fmin(box0.minimum.y(), box1.minimum.y()),
      fmin(box0.minimum.z(), box1.minimum.z())
    );

    Point3 max(
      fmax(box0.maximum.x(), box1.maximum.x()),
      fmax(box0.maximum.y(), box1.maximum.y()),
      fmax(box0.maximum.z(), box1.maximum.z())
    );

    return AABB(min, max);
  }

  Point3 minimum;
  Point3 maximum;
};

#endif // AABB_HPP