#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <random>

class Random
{
 public:
  /**
   * \brief Generate a random number between a min and max value (range is inclusive)
   * \tparam T Type of number to generate, only supports built in types (int, float, double, etc.)
   * \param min Minimum value
   * \param max Maximum value
   * \return Randomly generated value
   */
  template<typename T>
  static T Range(T min, T max)
  {
    std::random_device random_device;
    using distribution = typename dist_selector<std::is_integral<T>::value, T>::type;
    distribution dist(min, max);

    return dist(random_device);
  }

  /**
   * \brief Generate a random number between 0.0 and 1.0 (range is inclusive)
   * \return Randomly generated value
   */
  static float64 Range()
  {
    return Range<float64>(0.0, 1.0);
  }

 private:
  template<bool is_integral, typename T>
  struct dist_selector;

  template<typename T>
  struct dist_selector<true, T>
  {
    using type = std::uniform_int_distribution<T>;
  };

  template<typename T>
  struct dist_selector<false, T>
  {
    using type = std::uniform_real_distribution<T>;
  };
};

#endif // RANDOM_HPP