
#ifndef ISOTROPIC_HPP
#define ISOTROPIC_HPP

#include "Common.hpp"
#include "material/Material.hpp"
#include "textures/Texture.hpp"
#include "textures/SolidColour.hpp"
#include "HitRecord.hpp"

struct Isotropic : public Material
{
  Isotropic(Colour col) : albedo(std::make_shared<SolidColour>(col)) { }
  Isotropic(std::shared_ptr<Texture> tex) : albedo(tex) { }

  virtual bool32 Scatter(const Ray& rayIn, const HitRecord& record, Colour& attenuation, Ray& scattered) const override
  {
    scattered = Ray(record.p, RandomVec3InUnitSphere(), rayIn.Time());
    attenuation = albedo->Value(record.u, record.v, record.p);
    return true;
  }

  std::shared_ptr<Texture> albedo;
};

#endif // ISOTROPIC_HPP