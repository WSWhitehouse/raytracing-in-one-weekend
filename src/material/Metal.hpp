#ifndef METAL_HPP
#define METAL_HPP

#include "../Common.hpp"
#include "Material.hpp"

struct Metal : public Material
{
  Metal(const Colour& albedo, double fuzz) : 
    albedo(albedo), fuzz(fuzz < 1 ? fuzz : 1) {}

  virtual ~Metal() = default;

  virtual bool32 Scatter(const Ray& rayIn, const HitRecord& record, Colour& attenuation, Ray& scattered) const override
  {
    Vec3 reflected = Reflect(UnitVector(rayIn.Direction()), record.normal);
    scattered      = Ray(record.p, reflected + fuzz * RandomVec3InUnitSphere(), rayIn.Time());
    attenuation    = albedo;
    return (Dot(scattered.Direction(), record.normal) > 0);
  }

  Colour albedo;
  double fuzz;
};

#endif // METAL_HPP