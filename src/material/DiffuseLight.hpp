#ifndef DIFFUSE_LIGHT_HPP
#define DIFFUSE_LIGHT_HPP

#include "Common.hpp"
#include "Material.hpp"
#include "textures/Texture.hpp"
#include "textures/SolidColour.hpp"

struct DiffuseLight : public Material
{
  DiffuseLight(std::shared_ptr<Texture> emit) : emit(emit) { }
  DiffuseLight(Colour col) : emit(std::make_shared<SolidColour>(col)) { }

  virtual bool32 Scatter(const Ray& rayIn, const HitRecord& record, Colour& attenuation, Ray& scattered) const override
  {
    return false;
  }

  virtual Colour Emitted(float64 u, float64 v, const Point3& p) const override
  {
    return emit->Value(u, v, p);
  }

  std::shared_ptr<Texture> emit;
};

#endif // DIFFUSE_LIGHT_HPP