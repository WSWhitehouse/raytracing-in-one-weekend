#ifndef LAMBERTIAN_HPP
#define LAMBERTIAN_HPP

#include "../Common.hpp"
#include "../HitRecord.hpp"
#include "Material.hpp"
#include "../textures/Texture.hpp"
#include "../textures/SolidColour.hpp"

struct Lambertian : public Material
{
  Lambertian(const Colour& albedo) : albedo(std::make_shared<SolidColour>(albedo)) { }
  Lambertian(std::shared_ptr<Texture> albedo) : albedo(albedo) { }

  virtual ~Lambertian() = default;

  virtual bool32 Scatter(const Ray& rayIn, const HitRecord& record, Colour& attenuation, Ray& scattered) const override
  {
    Vec3 scatterDirection = record.normal + RandomVec3UnitVector();
    if (scatterDirection.NearZero()) scatterDirection = record.normal;

    scattered   = Ray(record.p, scatterDirection, rayIn.Time());
    attenuation = albedo->Value(record.u, record.v, record.p);
    return true;
  }

  std::shared_ptr<Texture> albedo;
};

#endif // LAMBERTIAN_HPP