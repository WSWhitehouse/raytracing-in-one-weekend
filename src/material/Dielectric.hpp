#ifndef DIELECTRIC_HPP
#define DIELECTRIC_HPP

#include "../Common.hpp"
#include "Material.hpp"
#include "../HitRecord.hpp"

struct Dielectric : public Material
{
  Dielectric(double indexOfRefraction) :
    indexOfRefraction(indexOfRefraction) {}

  virtual ~Dielectric() = default;  

  virtual bool32 Scatter(const Ray& rayIn, const HitRecord& record, Colour& attenuation, Ray& scattered) const override
  {
    attenuation             = Colour(1.0, 1.0, 1.0);
    float64 refractionRatio = record.frontFace ? (1.0 / indexOfRefraction) : indexOfRefraction;

    Vec3 unitDirection = UnitVector(rayIn.Direction());
    float64 cosTheta   = fmin(Dot(-unitDirection, record.normal), 1.0);
    float64 sinTheta   = sqrt(1.0 - cosTheta * cosTheta);

    bool32 cannotRefract = refractionRatio * sinTheta > 1.0;
    Vec3 direction;

    if (cannotRefract || Reflectance(cosTheta, refractionRatio) > Random::Range())
    {
      direction = Reflect(unitDirection, record.normal);
    }
    else
    {
      direction = Refract(unitDirection, record.normal, refractionRatio);
    }

    scattered = Ray(record.p, direction, rayIn.Time());
    return true;
  }

  double indexOfRefraction;

 private:
  static float64 Reflectance(float64 cosine, float64 refIndex)
  {
    // NOTE(WSWhitehouse): Using Schlick's approximation for reflectance
    float64 r0 = (1 - refIndex) / (1 + refIndex);
    r0 = r0 * r0;
    return r0 + (1 - r0) * pow((1 - cosine), 5);
  }
};

#endif // DIELECTRIC_HPP