#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "../Common.hpp"

struct HitRecord;

struct Material
{
  Material()          = default;
  virtual ~Material() = default;

  virtual bool32 Scatter(const Ray& rayIn, const HitRecord& record, Colour& attenuation, Ray& scattered) const = 0;
  virtual Colour Emitted(float64 u, float64 v, const Point3& p) const { return Colour(0.0, 0.0, 0.0); }
};

#endif // MATERIAL_HPP
