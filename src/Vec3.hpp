#ifndef VEC3_HPP
#define VEC3_HPP

#include <cmath>
#include <iostream>
#include "Random.hpp"

using std::sqrt;

struct Vec3 
{
  Vec3() : e{0,0,0} {}
  Vec3(float64 e0, float64 e1, float64 e2) : e{e0, e1, e2} {}

  const float64& x() const { return e[0]; }
  const float64& y() const { return e[1]; }
  const float64& z() const { return e[2]; }

  inline Vec3 operator-() const  { return Vec3(-e[0], -e[1], -e[2]); }
  float64 operator[](int i) const { return e[i]; }
  float64& operator[](int i)      { return e[i]; }

  Vec3& operator+=(const Vec3 &v) 
  {
    e[0] += v.e[0];
    e[1] += v.e[1];
    e[2] += v.e[2];
    return *this;
  }

  Vec3& operator*=(const float64 t) 
  {
    e[0] *= t;
    e[1] *= t;
    e[2] *= t;
    return *this;
  }

  Vec3& operator/=(const float64 t) 
  {
    return *this *= 1/t;
  }

  float64 Length() const        { return sqrt(LengthSquared()); }
  float64 LengthSquared() const { return e[0]*e[0] + e[1]*e[1] + e[2]*e[2]; }

  bool32 NearZero() const
  {
    const float64 epsilon = 1e-8;
    return (fabs(e[0]) < epsilon) && (fabs(e[1]) < epsilon) && (fabs(e[2] < epsilon));
  }
  
  double e[3];
};

// Type aliases for vec3
typedef Vec3 Point3;
typedef Vec3 Colour;

// vec3 Util

inline std::ostream& operator<<(std::ostream &out, const Vec3 &v) 
{
  return out << v.e[0] << ' ' << v.e[1] << ' ' << v.e[2];
}

inline Vec3 operator+(const Vec3 &u, const Vec3 &v) 
{
  return Vec3(u.e[0] + v.e[0], u.e[1] + v.e[1], u.e[2] + v.e[2]);
}

inline Vec3 operator-(const Vec3 &u, const Vec3 &v) 
{
  return Vec3(u.e[0] - v.e[0], u.e[1] - v.e[1], u.e[2] - v.e[2]);
}

inline Vec3 operator*(const Vec3 &u, const Vec3 &v) 
{
  return Vec3(u.e[0] * v.e[0], u.e[1] * v.e[1], u.e[2] * v.e[2]);
}

inline Vec3 operator*(float64 t, const Vec3 &v) 
{
  return Vec3(t*v.e[0], t*v.e[1], t*v.e[2]);
}

inline Vec3 operator*(const Vec3 &v, float64 t) 
{
  return t * v;
}

inline Vec3 operator/(Vec3 v, float64 t) 
{
  return (1/t) * v;
}

inline double Dot(const Vec3 &u, const Vec3 &v)
{
  return u.e[0] * v.e[0]
       + u.e[1] * v.e[1]
       + u.e[2] * v.e[2];
}

inline Vec3 Cross(const Vec3 &u, const Vec3 &v) 
{
  return Vec3(u.e[1] * v.e[2] - u.e[2] * v.e[1],
              u.e[2] * v.e[0] - u.e[0] * v.e[2],
              u.e[0] * v.e[1] - u.e[1] * v.e[0]);
}

inline Vec3 UnitVector(Vec3 v) { return v / v.Length(); }

inline Vec3 Reflect(const Vec3& vec, const Vec3& normal) { return vec - 2 * Dot(vec, normal) * normal; }

inline Vec3 Refract(const Vec3& uv, const Vec3& normal, float64 etaiOverEtat)
{
  float64 cosTheta  = fmin(Dot(-uv, normal), 1.0);
  Vec3 rOutPerp     = etaiOverEtat * (uv + cosTheta * normal);
  Vec3 rOutParallel = -sqrt(fabs(1.0 - rOutPerp.LengthSquared())) * normal;
  return rOutPerp + rOutParallel;
}

inline Vec3 RandomVec3() { return Vec3(Random::Range(), Random::Range(), Random::Range()); }

inline Vec3 RandomVec3(float64 min, float64 max)
{
  return Vec3(Random::Range<float64>(min, max), 
              Random::Range<float64>(min, max), 
              Random::Range<float64>(min, max));
}

inline Vec3 RandomVec3InUnitSphere()
{
  while(true)
  {
    Vec3 point = RandomVec3(-1.0, 1.0);
    if (point.LengthSquared() >= 1) continue;
    return point;
  }
}

inline Vec3 RandomVec3UnitVector() { return UnitVector(RandomVec3InUnitSphere()); } 

inline Vec3 RandomVec3InHemisphere(const Vec3& normal)
{
  Vec3 inUnitSphere = RandomVec3InUnitSphere();

  // NOTE(WSWhitehouse): Is in the same hemisphere as the normal
  return Dot(inUnitSphere, normal) > 0.0 ? inUnitSphere : -inUnitSphere;
}

inline Vec3 RandomInUnitDisk()
{
  while (true)
  {
    Vec3 point = Vec3(Random::Range(-1.0, 1.0), Random::Range(-1.0, 1.0), 0.0);
    if (point.LengthSquared() >= 1) continue;
    return point; 
  }
}

#endif // VEC3_HPP