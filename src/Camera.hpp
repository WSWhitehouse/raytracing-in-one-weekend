#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "Common.hpp"
#include "ImageData.hpp"

struct CameraData
{
  Point3 position     = Point3(13.0, 2.0, 3.0);
  Point3 lookAt       = Point3(0.0, 0.0, 0.0);
  Vec3 viewUp         = Vec3(0.0, 1.0, 0.0);
  float64 verticalFOV = 20.0; // NOTE(WSWhitehouse): In degrees
  float64 focusDist   = 10.0;
  float64 aperture    = 0.1;
  float64 startTime   = 0.0;
  float64 endTime     = 1.0;
};

struct Camera
{
  Camera(CameraData& camData, ImageData& imageData) 
    : Camera(camData.position, camData.lookAt, camData.viewUp, camData.verticalFOV, imageData.aspectRatio, 
             camData.aperture, camData.focusDist, camData.startTime, camData.endTime) { }

  Camera(Point3 position, Point3 lookAt, Vec3 viewUp, float64 verticalFOV, float64 aspectRatio, 
         float64 aperture, float64 focusDist, float64 _time0, float64 _time1)
  {
    float64 theta          = DegreesToRadians(verticalFOV);
    float64 h              = tan(theta * 0.5);
    float64 viewportHeight = 2.0 * h;
    float64 viewportWidth  = aspectRatio * viewportHeight;

    w = UnitVector(position - lookAt);
    u = UnitVector(Cross(viewUp, w));
    v = Cross(w, u);
    
    origin          = position;
    horizontal      = focusDist * viewportWidth * u;
    vertical        = focusDist * viewportHeight * v;
    lowerLeftCorner = origin - (horizontal * 0.5) - (vertical * 0.5) - (focusDist * w);
    lensRadius      = aperture * 0.5;

    time0 = _time0;
    time1 = _time1;
  }

  Ray GetRay(double s, double t)
  {
    Vec3 rd      = lensRadius * RandomInUnitDisk();
    Vec3 offset  = u * rd.x() + v * rd.y();
    float64 time = Random::Range<float64>(time0, time1);

    return Ray(origin + offset, lowerLeftCorner + s * horizontal + t * vertical - origin - offset, time);
  }

 private:
  Point3 origin;
  Vec3 horizontal;
  Vec3 vertical;
  Vec3 lowerLeftCorner;
  Vec3 u, v, w;
  float64 lensRadius;
  float64 time0, time1; // NOTE(WSWhitehouse): Cam shutter open/close times
};

#endif // CAMERA_HPP