#ifndef STB_IMAGE_WRAPPER_HPP
#define STB_IMAGE_WRAPPER_HPP

// NOTE(WSWhitehouse):
// Using a wrapper file here for STB Image to remove warnings and to ensure the implementation is included.
// Include this file when using the STB Image libary rather than including the file from vendor.

// Disable pedantic warnings for this external library.
#ifdef _MSC_VER
    // Microsoft Visual C++ Compiler
    #pragma warning (push, 0)
#endif

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

// Restore warning levels.
#ifdef _MSC_VER
    // Microsoft Visual C++ Compiler
    #pragma warning (pop)
#endif


#endif // STB_IMAGE_WRAPPER_HPP