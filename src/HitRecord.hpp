#ifndef HIT_RECORD_HPP
#define HIT_RECORD_HPP

#include "Common.hpp"

struct Material;

struct HitRecord
{
  Point3 p;
  Vec3 normal;
  std::shared_ptr<Material> materialPtr;
  float64 t;
  float64 u, v;
  bool32 frontFace;

  inline void SetFaceNormal(const Ray& ray, const Vec3& outwardNormal)
  {
    frontFace = Dot(ray.Direction(), outwardNormal) < 0;
    normal    = frontFace ? outwardNormal : -outwardNormal;
  }
};

#endif // HIT_RECORD_HPP