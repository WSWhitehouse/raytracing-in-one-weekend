@echo off

REM Runs the application - requires the executable to be built first using build.bat
REM Sends the output to image.ppm to genereate an image from std::cout 

build\raytracing.exe > build\image.ppm